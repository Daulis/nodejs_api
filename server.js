const express = require('express')
const dotenv = require('dotenv')
const app = express()
const bodyParser = require('body-parser')

dotenv.config()

const Categorie = require('./app/router/categorie')
const Product = require('./app/router/product')

require('./app/database/db_config')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json({ limit: '1000mb' }))
app.use(express.json())

app.get('/', (req, res) => {
  res.send(`<h1>Hello API</h1>`)
})

app.use('/api/categorie', Categorie)
app.use('/api/product', Product)

app.listen(process.env.PORT, () => {
  console.log(`Server listen on port ${process.env.PORT}...`)
})

console.log('----------------------------------------------')
console.log('-----------------🚀API-BACKEND----------------')
console.log('----------------------------------------------')
