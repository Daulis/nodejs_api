const { Schema, model } = require('mongoose')

const productSchema = new Schema({
  name: {
    type: String,
    minLength: 2,
    maxLength: 100,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  categorieId: {
    type: Schema.Types.ObjectId,
    ref: 'Categorie',
  },
  createdAt: {
    type: String,
  },
  updatedAt: {
    type: String,
  },
})

module.exports = model('product', productSchema)
