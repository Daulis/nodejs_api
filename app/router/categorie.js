const {
  createCategory,
  getAllCategories,
  getOneCategorie,
  updateCategorie,
  deleteCategorie,
} = require('../controllers/Categorie')

const express = require('express')
const Router = express.Router()

Router.post('/add_categorie', createCategory)
Router.get('/', getAllCategories)
Router.get('/:id', getOneCategorie)
Router.put('/update_categorie/:id', updateCategorie)
Router.delete('/delete_categorie/:id', deleteCategorie)

module.exports = Router
