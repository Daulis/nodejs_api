const {
  createProduct,
  getAllProducts,
  getOneProduct,
  updateProduct,
  deleteProduct,
} = require('../controllers/Product')

const express = require('express')
const Router = express.Router()

Router.post('/add_product', createProduct)
Router.get('/', getAllProducts)
Router.get('/one_product/:id', getOneProduct)
Router.put('/update_product/:id', updateProduct)
Router.delete('/delete/:id', deleteProduct)

module.exports = Router
