const mongoose = require('mongoose')
mongoose
  .connect('mongodb://localhost:27017/voiture', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('Database connected!'))
  .catch((err) => console.log('Failed to connect', err))
