const ProductModel = require('../models/Product')
const ObjectID = require('mongoose').Types.ObjectId

module.exports = {
  createProduct: async (req, res) => {
    const { name, description, price, categorieId } = req.body

    try {
      const product = await new ProductModel({
        name,
        description,
        price,
        categorieId,
        createdAt: new Date().toISOString(),
      })
      product.save()
      return res.status(201).json({ product: product.name })
    } catch (error) {
      return res.status(400).send({ error })
    }
  },
  getAllProducts: async (req, res) => {
    try {
      const products = await ProductModel.find().populate({
        path: 'categorieId',
        model: 'categorie',
      })
      return res.status(200).json({ products })
    } catch (error) {
      return res.status(400).send({ error })
    }
  },
  getOneProduct: (req, res) => {
    const productId = req.params.id
    if (!ObjectID.isValid(productId)) {
      return res.status(400).send('Product not found:' + productId)
    }
    ProductModel.findById(productId, (err, data) => {
      if (!err) {
        return res.status(200).json({ data })
      } else {
        console.log(err)
      }
    })
      .clone()
      .populate({ path: 'categorieId', model: 'categorie' })
      .exec()
  },

  updateProduct: async (req, res) => {
    const productId = req.params.id
    const { name, description, price, categorieId } = req.body

    if (!ObjectID.isValid(productId)) {
      return res.status(400).send('Product not found:' + productId)
    }

    await ProductModel.findByIdAndUpdate(
      { _id: productId },
      {
        $set: {
          name,
          description,
          price,
          categorieId,
          updatedAt: new Date().toISOString(),
        },
      },
      { new: true, upsert: true, setDefaultOnInsert: true },
      (err, docs) => {
        if (!err) return res.status(200).send({ docs })
        return res.status(500).json({ message: err })
      },
    )
  },
  deleteProduct: async (req, res) => {
    const productId = req.params.id

    if (!ObjectID.isValid(productId)) {
      return res.status(400).send('Product not found:' + productId)
    }
    try {
      await ProductModel.deleteOne({ _id: productId }).exec()
      return res.status(200).json({ message: 'Product delete successfuly' })
    } catch (error) {
      return res.status(500).send({ error })
    }
  },
}
