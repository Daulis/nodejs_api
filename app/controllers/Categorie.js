const CategorieModel = require('../models/Categorie')
const ObjectID = require('mongoose').Types.ObjectId

module.exports = {
  createCategory: async (req, res) => {
    const { name } = req.body
    try {
      const categorie = await new CategorieModel({
        name,
        createdAt: new Date().toISOString(),
      })
      categorie.save()
      return res.status(201).json({ categorie: categorie.name })
    } catch (error) {
      return res.status(400).send({ error })
    }
  },
  getAllCategories: async (req, res) => {
    try {
      const categories = await CategorieModel.find()
      return res.status(200).json({ categories })
    } catch (error) {
      return res.status(400).json({ error })
    }
  },
  getOneCategorie: (req, res) => {
    const categorieId = req.params.id

    if (!ObjectID.isValid(categorieId)) {
      return res.status(400).send('Categorie Not Found===' + categorieId)
    }

    CategorieModel.findById(categorieId, (err, docs) => {
      if (!err) {
        return res.status(200).json({ docs })
      } else {
        console.log('Categorie not found')
      }
    })
  },
  updateCategorie: (req, res) => {
    const categorieId = req.params.id
    const { name } = req.body
    if (!ObjectID.isValid(categorieId)) {
      return res.status(400).send('Categorie Not Found===' + categorieId)
    }

    CategorieModel.findByIdAndUpdate(
      { _id: categorieId },
      {
        $set: {
          name,
          updatedAt: new Date().toISOString(),
        },
      },
      { new: true, upsert: true, setDefaultsOnInsert: true },
      (err, data) => {
        if (!err) {
          return res.status(200).send({ data })
        }
        if (err) return res.status(500).send({ message: err })
      },
    )
  },
  deleteCategorie: async (req, res) => {
    const categorieId = req.params.id
    if (!ObjectID.isValid(categorieId)) {
      return res.status(400).send('Categorie Not Found===' + categorieId)
    }

    try {
      await CategorieModel.deleteOne({ _id: categorieId }).exec()
      return res.status(200).json({ message: 'Categorie delete successfuly' })
    } catch (error) {
      return res.status(500).json({ error })
    }
  },
}
